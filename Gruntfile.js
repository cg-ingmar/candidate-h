const sass = require('node-sass');

module.exports = function(grunt) {

     'use strict';

    /**
     *
     *        Install dependencies:     npm install
     *
     *             When developing:     grunt dev
     *              For production:     grunt build
     *
    **/
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        project: {
            name: '<%= pkg.name %>'
        },

        // SASS
        sass: {
            options: {
                outputStyle: 'compressed',
                implementation: sass,
                sourceComments: false,
                sourceMap: false,
                includePaths: [
                    'node_modules'
                ]
            },
            front: {
                files: [{
                    src: 'styles/styles-screen.scss',
                    dest: 'assets/styles-screen.css'
                }]
            }
        },

        // Watch
        watch: {
            sass: {
                files: ['styles/**/*.scss'],
                tasks: ['sass']
            },
        },

    });

    // Load Npm Tasks

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-newer');

    // Tasks
    grunt.registerTask('dev', ['watch']);
    grunt.registerTask('build', ['sass']);
};
